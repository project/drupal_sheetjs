<?php

namespace Drupal\drupal_sheetjs\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drupal_sheetjs\Services\DrupalSheetjsConfigService;

class DrupalSheetjsController extends ControllerBase
{
  /**
   * @return array
   */
  public function test () {
    $drupal_sheetjs_config = $this->_getDrupalSheetjsConfigDataService()->getData();
    unset($drupal_sheetjs_config['data']['full_data']);
    return [
      '#theme' => 'test',
      '#drupal_sheetjs_config' => $drupal_sheetjs_config,
    ];
  }

  /**
   * @return array
   */
  public function test_custom () {
    $drupal_sheetjs_config = $this->_getDrupalSheetjsConfigDataService()->getData('is_test');
    unset($drupal_sheetjs_config['data']['full_data']);
    return [
      '#theme' => 'test_custom',
      '#drupal_sheetjs_config' => $drupal_sheetjs_config,
    ];
  }

  /**
   * @return DrupalSheetjsConfigService
   */
  private function _getDrupalSheetjsConfigDataService() {
    return new DrupalSheetjsConfigService();
  }

}
