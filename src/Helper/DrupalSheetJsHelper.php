<?php

namespace Drupal\drupal_sheetjs\Helper;

class DrupalSheetJsHelper
{
  public static $dest_table_id          = 'exportTableId';
  public static $submit_element_id      = 'submitButtonId';
  public static $submit_full_element_id = 'submitFullButtonId';
  public static $current_file_name      = 'SheetJSTableExport';
  public static $full_data_file_name    = 'SheetJSFullDataTableExport';
  public static $export_type            = 10;
}
