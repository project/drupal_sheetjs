<?php

namespace Drupal\drupal_sheetjs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drupal_sheetjs\Helper\DrupalSheetJsHelper as Helper;

/**
 * Configure Drupal SheetJS settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * @return string
   */
  public function getFormId() {
    return 'drupal_sheetjs_settings';
  }

  /**
   * @return string[]
   */
  protected function getEditableConfigNames() {
    return ['drupal_sheetjs.settings'];
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('drupal_sheetjs.settings');
    $form['dest_table'] = [
      '#type' => 'details',
      '#title' => $this->t('Specified export table'),
      '#open' => TRUE,
    ];
    $form['dest_table']['dest_table_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Export table\'s ID'),
      '#default_value' => !empty($config->get('dest_table_id')) ? $config->get('dest_table_id') : Helper::$dest_table_id,
      '#maxlength' => NULL,
    ];
    $form['submit_element'] = [
      '#type' => 'details',
      '#title' => $this->t('Submit button'),
      '#open' => TRUE,
    ];
    $form['submit_element']['submit_element_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit button ID'),
      '#default_value' => !empty($config->get('submit_element_id')) ? $config->get('submit_element_id') : Helper::$submit_element_id,
      '#maxlength' => NULL,
    ];
    $form['submit_element']['submit_full_element_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit full data button ID'),
      '#default_value' => !empty($config->get('submit_full_element_id')) ? $config->get('submit_full_element_id') : Helper::$submit_full_element_id,
      '#maxlength' => NULL,
    ];
    $form['export_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Export type settings'),
      '#open' => TRUE,
    ];
    $form['export_settings']['export_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Set your export type'),
      '#default_value' => !in_array($config->get('export_type'), [10, 20]) ? 10 : $config->get('export_type'),
      '#options' => [10 => $this->t('xlsx'), 20 => $this->t('csv')],
    ];
    $form['template_set'] = [
      '#type' => 'details',
      '#title' => $this->t('Template set'),
      '#open' => TRUE,
    ];
    $form['template_set']['current_file_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Current file name'),
      '#default_value' => !empty($config->get('current_file_name')) ? $config->get('current_file_name') : Helper::$current_file_name,
      '#maxlength' => NULL,
    ];
    $form['template_set']['full_data_file_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Current file name'),
      '#default_value' => !empty($config->get('full_data_file_name')) ? $config->get('full_data_file_name') : Helper::$full_data_file_name,
      '#maxlength' => NULL,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $pattern = '/^[A-Za-z]+[\w\-\:\.]*$/';

    $dest_table_id_invalidate_msg = 'Export table ID is not a validate CSS ID';
    $submit_element_id_invalidate_msg = 'Submit button ID is not a validate CSS ID.';

    $dest_table_id_empty_msg = 'Export table ID must be not empty';
    $submit_element_id_empty_msg = 'Submit button ID must be not empty';
    $submit_full_element_id_empty_msg = 'Submit full data button ID must be not empty';
    $current_file_name_empty_msg = 'Current table file name must be not empty';
    $full_data_file_name_empty_msg = 'Full data file name must be not empty';


    $dest_table_id = $this->_getDestTableId($form_state);
    $submit_element_id = $this->_getSubmitElementId($form_state);
    $submit_full_element_id = $this->_getSubmitFullElementId($form_state);
    $current_file_name = $this->_getCurrentFileName($form_state);
    $full_data_file_name = $this->_getFullDataFileName($form_state);

    if ( empty( $dest_table_id ) ) {
      $form_state->setErrorByName('dest_table_id', $this->t($dest_table_id_empty_msg));
    }

    if ( empty( $submit_element_id ) ) {
      $form_state->setErrorByName('submit_element_id', $this->t($submit_element_id_empty_msg));
    }

    if ( empty( $submit_full_element_id ) ) {
      $form_state->setErrorByName( 'submit_full_element_id', $this->t( $submit_full_element_id_empty_msg ) );
    }

    if ( empty( $current_file_name ) ) {
      $form_state->setErrorByName( 'current_file_name', $this->t( $current_file_name_empty_msg ) );
    }

    if ( empty( $full_data_file_name ) ) {
      $form_state->setErrorByName( 'full_data_file_name', $this->t( $full_data_file_name_empty_msg ) );
    }

    $this->_cssIdValidator (
      $dest_table_id,
      $pattern,
      'dest_table_id',
      $dest_table_id_invalidate_msg,
      $form_state
    );

    $this->_cssIdValidator (
      $submit_element_id,
      $pattern,
      'submit_element_id',
      $submit_element_id_invalidate_msg,
      $form_state
    );

    $this->_cssIdValidator (
      $submit_full_element_id,
      $pattern,
      'submit_full_element_id',
      $submit_full_element_id_empty_msg,
      $form_state
    );

    parent::validateForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $dest_table_id =  $this->_getDestTableId($form_state) ;
    $submit_element_id = $this->_getSubmitElementId($form_state);
    $submit_full_element_id = $this->_getSubmitFullElementId($form_state);

    $this->config('drupal_sheetjs.settings')
      ->set('dest_table_id', $dest_table_id)
      ->set('submit_element_id', $submit_element_id)
      ->set('submit_full_element_id', $submit_full_element_id)
      ->set('export_type', $form_state->getValue('export_type'))
      ->set('current_file_name', $form_state->getValue('current_file_name'))
      ->set('full_data_file_name', $form_state->getValue('full_data_file_name'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * @param string $subject
   * @param string $pattern
   * @param string $validate_field * @param string $msg
   * @param FormStateInterface $form_state
   */
  private function _cssIdValidator (
    string $subject,
    string $pattern,
    string $validate_field,
    string $msg,
    FormStateInterface $form_state
  ) {
    $result = preg_match($pattern, $subject);
    if (false === $result || 0 === $result) {
      $form_state->setErrorByName($validate_field, $this->t($msg));
    }
  }

  /**
   * @param FormStateInterface $form_state
   * @return string
   */
  private function _getDestTableId( FormStateInterface $form_state ) {
    return trim ( $form_state->getValue('dest_table_id') ) ;
  }

  /**
   * @param FormStateInterface $form_state
   * @return string
   */
  private function _getSubmitElementId( FormStateInterface $form_state ) {
    return trim ( $form_state->getValue('submit_element_id') ) ;
  }

  /**
   * @param FormStateInterface $form_state
   * @return string
   */
  private function _getSubmitFullElementId( FormStateInterface $form_state ) {
    return trim ( $form_state->getValue('submit_full_element_id') ) ;
  }

  /**
   * @param FormStateInterface $form_state
   * @return string
   */
  private function _getCurrentFileName( FormStateInterface $form_state ) {
    return trim ( $form_state->getValue('current_file_name') ) ;
  }

  /**
   * @param FormStateInterface $form_state
   * @return string
   */
  private function _getFullDataFileName( FormStateInterface $form_state ) {
    return trim ( $form_state->getValue('full_data_file_name') ) ;
  }

}
