<?php

namespace Drupal\drupal_sheetjs\Services;

use Drupal\drupal_sheetjs\Helper\DrupalSheetJsHelper as Helper;

class DrupalSheetjsConfigService
{

  public function getData ( $page_alias = 'default' ) {
    $data = $this->_getDefaultData();
    if ( 'is_test' === $page_alias ) {
      $data = $this->_getTestData();
    }
    return $data;
  }

  private function _getDefaultData () {
    $config = \Drupal::config('drupal_sheetjs.settings');

    $_types = [10 => 'xlsx', 20 => 'csv'];
    $_types_key = array_keys($_types);
    $export_type = $config->get('export_type');

    $full_data = [];

    return [
      'data' => [
        'dest_table_id' => !empty($config->get('dest_table_id')) ? $config->get('dest_table_id') : Helper::$dest_table_id,
        'submit_element_id' => !empty($config->get('submit_element_id')) ? $config->get('submit_element_id'): Helper::$submit_element_id,
        'submit_full_element_id' => !empty($config->get('submit_full_element_id')) ? $config->get('submit_full_element_id') : Helper::$submit_full_element_id,
        'current_file_name' => !empty($config->get('current_file_name')) ? $config->get('current_file_name') : Helper::$current_file_name,
        'full_data_file_name' => !empty($config->get('full_data_file_name')) ? $config->get('full_data_file_name') : Helper::$full_data_file_name,
        'export_type' => in_array($export_type, $_types_key) ? $_types[$export_type] : "xlsx",
        'full_data' => $full_data,
      ],
    ];
  }

  private function _getTestData () {
    $title = [ ['title01', 'title02', 'title03',] ];
    $data = [
      ['data01-01', 'data01-02', 'data01-03',],
      ['data02-01', 'data02-02', 'data02-03',],
      ['data03-01', 'data03-02', 'data03-03',],
    ];
    $full_data = array_merge($title, $data);
    return [
      'data' => [
        'dest_table_id' => 'tableId',
        'submit_element_id' => 'submitId',
        'submit_full_element_id' => 'submitFullId',
        'export_type' => "csv",
        'current_file_name' => 'SheetJsCurrentFile',
        'full_data_file_name' => 'SheetJsFullDataFile',
        'full_data' => $full_data,
      ],
    ];
  }

}
