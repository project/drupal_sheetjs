<?php


namespace Drupal\drupal_sheetjs\EventSubscriber;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\drupal_sheetjs\Services\DrupalSheetjsConfigService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class DrupalSheetjsEventSubscriber
 * @package Drupal\drupal_sheetjs\EventSubscriber
 */
class DrupalSheetjsEventSubscriber implements EventSubscriberInterface
{


  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(AccountInterface $currentUser, UrlGeneratorInterface $urlGenerator, RendererInterface $renderer) {
    $this->currentUser = $currentUser;
    $this->urlGenerator = $urlGenerator;
    $this->renderer = $renderer;
  }

  /**
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   */
  public function onKernelResponse(FilterResponseEvent $event)
  {
    $response = $event->getResponse();
    $request = $event->getRequest();

    // Do not capture redirects or modify XML HTTP Requests.
    if ($request->isXmlHttpRequest()) {
      return;
    }

    if ($this->currentUser->hasPermission('access drupal sheetjs')) {
      $this->injectDrupalSheetjs($response);
    }
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Response $response
   */
  protected function injectDrupalSheetjs(Response $response)
  {
    $content = $response->getContent();
    $pos = mb_strripos($content, '</body>');

    /**
     * Data with no pagination for download
     *
     * Data structure sample below:
     *
     * -------------------------------
     * | title01 | title02 | title03 |
     * -------------------------------
     * | data01  | data02  | data03  |
     * | data11  | data12  | data13  |
     * | data21  | data22  | data23  |
     * -------------------------------
     */
    $full_data = [];

    if (FALSE !== $pos) {
      $loader = [
        '#theme' => "drupal_sheetjs_loader",
        '#drupal_sheetjs_config' => (new DrupalSheetjsConfigService())->getData(),
      ];
      $content = mb_substr($content, 0, $pos) . $this->renderer->renderRoot($loader) . mb_substr($content, $pos);
      $response->setContent($content);
    }
  }

  /**
   * @return array
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse', -120],
    ];
  }

}
