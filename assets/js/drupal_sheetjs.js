(function(){
  DrupalSheetJs = {
    test: function () {
      if ( this.checkXlsxLoaded() ) {
        console.log( 'XLSX loaded successful' );
        return;
      }
      console.log( 'SheetJs lib not load' );
    },
    checkXlsxLoaded: function () {
      return XLSX && XLSX.version;
    },
    fileNameFilter: function ( fileName ) {
      if ( 'string' === typeof fileName ) {
        return fileName.replace(/[^a-zA-Z0-9\-\_]/gm, "");
      }
      return ''
    },
    currentTableDownload: function ( table_id, submit_element_id, export_type, file_name ) {
      var subDom = document.getElementById(submit_element_id);
      var elt = document.getElementById(table_id);
      if (this.checkXlsxLoaded()) {
        if (subDom && elt) {
          subDom.onclick = function () {
            if ( 'string' !== typeof file_name || file_name.length <= 0) {
              file_name = 'SheetJSTableExport'
            }
            file_name = this.fileNameFilter( file_name );
            var wb = XLSX.utils.table_to_book(elt, {sheet:"Sheet JS"});
            var export_file_name = file_name + '.' + export_type;
            /* eslint-disable */
            XLSX.writeFile(wb, export_file_name );
            /* eslint-enable */
          }
        } else {
          // console.log('Dom not found');
        }
      } else {
        // console.log('SheetJs library not loaded');
      }
    },
    fullDataDownload: function ( full_data, submit_element_id, export_type, file_name ) {
      if (this.checkXlsxLoaded()) {
        var subDom = document.getElementById(submit_element_id);
        if ( subDom ) {
          subDom.onclick = function () {
            if ( 'string' !== typeof file_name || file_name.length <= 0) {
              file_name = 'SheetJSTableFullDataExport'
            }
            file_name = this.fileNameFilter( file_name );
            var wb = XLSX.utils.book_new();
            var ws = XLSX.utils.aoa_to_sheet( full_data );
            XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
            export_type = export_type || 'xlsx';
            var export_file_name = file_name + '.' + export_type;
            /* eslint-disable */
            XLSX.writeFile(wb, export_file_name );
            /* eslint-enable */
          }
        } else {
          // console.log('Dom not found');
        }
      } else {
        // console.log('SheetJs library not loaded');
      }
    }
  };
})();
